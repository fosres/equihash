#if 0
https://blog.sigmaprime.io/zcash-theoretically-improving-mining-speeds.html
#endif

#include <stdio.h>
#include <cstring>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <sodium.h>
#include <stdbool.h>
#include <iostream>
#include <string>
#include <map>
#include <vector>

using namespace std;


#if 0
When it fails to find a node, returns NULL

So when it finds a hit, it returns the btree ** node
#endif


typedef struct sol
{
	vector<string*> sol_ref;
	
	string sol_str;

	string hash;
}sol;

string char_arr_to_string(unsigned char * x,unsigned long long int size)
{
	unsigned long long int i = 0;

	string res = "";

	while ( i < size )
	{
		res += x[i];

		i++;
	}	

	return res;
}

unsigned char first_l_bits(unsigned char a,unsigned char l)
{
	return a & ( ( (~0) << (8-l) ) & 0xff );
}

unsigned char last_l_bits(unsigned  char a,unsigned l)
{
	return a & ( 0xff >> (8-l) );
}

sol * xor_hashes(map<string,sol*>::iterator & it1,map<string,sol*>::iterator & it2,unsigned level)
{
//	cout << "Made it in xor_hashes" << endl;

	unsigned long long int i = 0;

	string output_hash = "";

	while	(
			i < (it1->first).size()
		)
	{
		output_hash += ( ( (it1->first)[i] ) ^ ( ( it2->first)[i] ) );

		i++;
	}

	sol * res = (sol*)calloc(1,sizeof(sol));
	
//	cout << "Made it right before res->sol_str check" << endl;

	res->hash = output_hash; // strings are passed by value--NOT reference
	
//	cout << "Made it right after res->sol_str check" << endl;

	if ( level == 1 )
	{
//		cout << "Made it to beginning of level == 1 in xor_hashes" << endl;

		res->sol_ref = vector<string*>();

//		cout << it1->second->sol_str << endl;

		(res->sol_ref).push_back( &( (it1->second)->sol_str ) );
		
		(res->sol_ref).push_back( &( (it2->second)->sol_str ) );
		
//		cout << "Made it to end of level == 1 in xor_hashes" << endl;
	}	
	
	else // for levels > 1 copy string* in each iterators' sol_ref vector
	{
//		res->sol_ref = (it1->second)->sol_ref; // vectors are deep copied when you use \'=\'

		unsigned long long int i = 0;
		
		while ( i < ( (it1->second)->sol_ref ).size() )
		{
			(res->sol_ref).push_back( ( (it1->second)->sol_ref )[i] );

			i++;
		}

		i = 0;

		while ( i < ( (it2->second)->sol_ref ).size() )
		{
			(res->sol_ref).push_back( ( (it2->second)->sol_ref )[i] );

			i++;
		}
	}	

	return res;
}

bool unique_hashes(sol *x,sol*y)
{
	map<string*,int> finder;

	unsigned long long int i = 0;

	while ( i < (x->sol_ref).size() )
	{
		finder.insert(finder.begin(),std::pair<string*,int>( (x->sol_ref)[i],1));

		i++;
	}

	i = 0;
	
	while ( i < (y->sol_ref).size() )
	{
		if ( finder.find( (y->sol_ref)[i]) != finder.end() )
		{
			break;
		}
		i++;
	}

	finder.clear();

	return i == (y->sol_ref).size();

}

void print_hash(string & x)
{
	unsigned long long int i = 0;

	unsigned char ans[x.size()];


	while ( i < x.size() )
	{
		printf("%.2x ",x[i]);

		i++;
	}

	putchar(0xa);
}

string num_to_hash(string & in,unsigned long long int n)
{
	unsigned char blake_hash[(unsigned long long int)ceil(n/8.0)];

	crypto_generichash(blake_hash,((unsigned long long int)ceil(n/8.0))*sizeof(unsigned char),&(in.front()),in.size(),NULL,0);

	unsigned long long int i = 0;

	string x = "";

	while ( i < ((unsigned long long int)ceil(n/8.0)) )
	{
		x += blake_hash[i];

		i++;
	}

	// take off excessive most significant bits

	x[0] = last_l_bits(x[0],n%8); // ( (0xff) & ( 0xff >> (8-n%8) ) );

	return x;
}


bool which_endian(unsigned long long int x,unsigned char bytearr[])
{
	// if little-endian return 1
	
	// if big-endian return 0

	unsigned char i = 0;

	while ( 
			( i < sizeof(unsigned long long int) )

			&&
			
			( ( x & 0xff ) == bytearr[i] )
	      )
	{
		x >>= 8;

		i++;
	}

	return i == 8;
	
}

void reverse_string(unsigned char * arr,unsigned long long int size)
{
	unsigned char i = 0, j = size - 1, temp = 0;

	while ( i < j )
	{
		temp = arr[i];

		arr[i] = arr[j];

		arr[j] = temp;
		
		i++;

		j--;
	}
	
}

string num_to_string(unsigned long long int x,unsigned long long int n,unsigned long long int k)
{
	//first check if big-endian or little-endian	

	unsigned char bytearray[(unsigned long long int)ceil(n/8.0)];

	memset(bytearray,0x00,((unsigned long long int)ceil(n/8.0))*sizeof(unsigned char));

	memcpy(bytearray,&x,sizeof(unsigned long long int));

	if ( which_endian(x,bytearray) == 1 )
	{
		reverse_string(bytearray,(unsigned long long int)ceil(n/8.0));	
	}

	// Just zero out excess 0 bits. This provides same effect as modulus operation since
	//
	// modulus dividend is a power of two	
	// This is only possible in the most significant byte

	bytearray[0] = last_l_bits(bytearray[0],n%8); // bytearray[0] & ( 0xff >> ( 8 - (n%8) ) );

	string h = "";
	
	unsigned long long int i = 0;
	
	while ( i < (unsigned long long int)ceil(n/8.0) )
	{
		h += bytearray[i];

		i++;	
	}

	return h;	
}

bool hasheq(map<string,sol*>::iterator & it1,map<string,sol*>::iterator & it2,unsigned long long int n,unsigned long long int k,unsigned level)
{
//	cout << "Made it in hasheq" << endl;

//	cout << "Level: " << level << endl;
	
	unsigned long long int first_bit = 0;
	
	if ( (n % 8) != 0 )
	{
		first_bit = 8 - (n%8);			
	}

	else
	{
		first_bit = 0;
	}

//	cout << "first_bit: " << first_bit << endl;
	
	unsigned long long int starting_bit = first_bit + (unsigned long long int)ceil((level*n)/(k+1.0));
	
	unsigned long long int starting_byte = starting_bit/8;

//	cout << starting_byte << endl;
	
	unsigned long long int ending_bit = first_bit + (unsigned long long int)ceil(( (level+1)*n)/(k+1.0));
	
	unsigned long long int ending_byte = ending_bit/8;

//	cout << ending_byte << endl;

	if ( level < (k-1) )
	{
		if ( 
			last_l_bits((it1->first)[starting_byte],8-( (starting_bit) % 8 ))

			!=
			
			last_l_bits((it2->first)[starting_byte],8-( (starting_bit) % 8 ))

		   )
		{
//			cout << "Its false" << endl;

			return false;	
		}

		unsigned long long int i = starting_byte + 1;

		while ( 
			( i < ending_byte )

			&&

			( (it1->first)[i] == (it2->first)[i] )

		      )
		{
			i++;
		}

		if ( i < ending_byte )
		{
			return false;
		}

//		cout << "Checking case: i: " << i << endl;

//		cout << "Ending byte: " << ending_byte << endl;

//		printf("it1 == it2: %d\n",it1 == it2);

//		printf("%d\n",first_l_bits((it1->first)[ending_byte],ending_bit % 8) == first_l_bits((it2->first)[ending_byte],ending_bit % 8));

		return first_l_bits((it1->first)[ending_byte],ending_bit % 8) == first_l_bits((it2->first)[ending_byte],ending_bit % 8);
	
	}
	
	else // level == (k-1) The level should never be exactly k
	{
		
//		cout << "In Level "  << (k-1) << endl;

		unsigned long long int j = 0;

//		cout << "Printing contents of it1->hash" << endl;

#if 0
		while ( j < (it1->first).size() )
		{
			printf("%.2x ",(it1->first)[j]);

			j++;
		}
#endif
		j = 0;
		
//		cout << "Printing contents of it2->hash" << endl;

#if 0
		while ( j < (it2->first).size() )
		{
			printf("%.2x ",(it2->first)[j]);

			j++;
		}
#endif
		unsigned long long int offset = n - (unsigned long long int)ceil( (2*n)/(k+1.0));

//		cout << "Offset: " << offset << endl;

		starting_bit = first_bit + offset;

		starting_byte = starting_bit / 8;

//		cout << "Starting byte " << starting_byte << endl;

		// There is an error here with comparing the last few bits. What are you going to do
		// when all bits need to be compared?

		if ( 
			last_l_bits((it1->first)[starting_byte],8-( (starting_bit) % 8 ))

			!=
			
			last_l_bits((it2->first)[starting_byte],8-( (starting_bit) % 8 ))

		   )
		{
//			cout << "Last_l_bits check failed" << endl;

			return false;	
		}

		unsigned long long int i = starting_byte + 1;

		while ( 
			( i < (it1->first).size() )

			&&

			( (it1->first)[i] == (it2->first)[i] )

		      )
		{
			i++;
		}

//		printf("Checking if i == (it1->first).size():%d\ni == %llu\n(it1->first).size() == %llu\n",i == (it1->first).size(),i,(it1->first).size());

		return i == (it1->first).size();

	}
}

// For finding collisions with hashes that are lexicographically greater, start from the
//
// right end of the tree.


vector<string*> * wagner(unsigned long long int n,unsigned long long int k,unsigned char * I,unsigned char * V,unsigned level,map<string,sol*> arr[],sol*ins)
{
	unsigned long long int i = 0;
	
	unsigned long long int N = (unsigned long long int)pow(2,ceil(n/(k+1.0)+1) ); 

	string iv = char_arr_to_string(I,17) + char_arr_to_string(V,21);
	
	string potential_sol = "", ans = "";
		
	sol * x = 0; 

	bool less_exists = 1, more_exists = 1;

	map<string,sol*>::iterator it1, it2, it3;

	vector<string*> * res = 0x00;

	if ( level == 0 )
	{
		arr[level] = map<string,sol*>();

		while ( i < (k+1) )
		{
			arr[i] = map<string,sol*>();

			i++;
		}

		i = 0;

		while ( i < N )
		{
			potential_sol.clear();

			potential_sol = num_to_string(i,n,k);
		
			x = (sol*)calloc(1,sizeof(sol));
			
			x->sol_str = potential_sol;

			potential_sol = iv + (x->sol_str);

			ans = num_to_hash(potential_sol,n);

			x->hash = ans;

//			print_hash(x->hash);

//			cout << "i: " << i << endl;

			ans.clear();

			x->sol_ref = vector<string*>();

			it3 = (arr[level]).insert( arr[level].begin(),std::pair<string,sol*>(x->hash,x) );

			it1 = it3;
	
			if ( it1 != (arr[level]).begin() )
			{
				it1--;
			}

			it2 = it3;

			it2++;

			while	(
					( it1 != (arr[level]).begin() )	

					&&

					(it1 != it3)

					&&

					( hasheq(it1,it3,n,k,level) == true )

				)
			{
				x = xor_hashes(it1,it3,level+1);

//				cout << "xor_hashes completed" << endl;

				if 	( 
						(res = wagner(n,k,I,V,level+1,arr,x) ) 
						
						!= 
						
						NULL 
					)
				{
					return res;	
				}	

				it1--;

			}

			if	(
					( it1 == (arr[level]).begin() )

					&&

					( it1 != it3 )

					&&

					( hasheq(it1,it3,n,k,level) == 1 )

				)
			{

				x = xor_hashes(it1,it3,level+1);

				if 	( 
						( res = wagner(n,k,I,V,level+1,arr,x) ) 
						
						!= 
						
						NULL 
					)
				{
					return res;	
				}	
			}

			
			while	(
					( it2 != (arr[level]).end() )	

					&&

					(it2 != it3)

					&&

					( hasheq(it2,it3,n,k,level) == 1 )

				)
			{

				
				x = xor_hashes(it2,it3,level+1);

				if 	( 
						(res = wagner(n,k,I,V,level+1,arr,x) ) 
						
						!= 
						
						NULL 
					)
				{
					return res;	
				}	

				it2++;

			}

			i++;
		}

		return NULL;
	}


	else if ( level < (k-1) )
	{

		it3 = (arr[level]).insert( arr[level].begin(),std::pair<string,sol*>(ins->hash,ins) );

		it1 = it3;

		if ( it1 != (arr[level]).begin() )
		{
			it1--;
		}
		
		it2 = it3;

		it2++;
		
		while	(
				( it1 != (arr[level]).begin() )	

				&&

				(it1 != it3)

				&&

				( hasheq(it1,it3,n,k,level) == 1 )

			)
		{
				x = xor_hashes(it1,it3,level+1);

				if 	( 
						(res = wagner(n,k,I,V,level+1,arr,x) ) 
						
						!= 
						
						NULL 
					)
				{
					return res;	
				}	

				it1--;

		}

		if	(
				( it1 == (arr[level]).begin() )

				&&

				(it1 != it3)

				&&

				( hasheq(it1,it3,n,k,level) == 1 )
			)
		{
				x = xor_hashes(it1,it3,level+1);

				if 	( 
						(res = wagner(n,k,I,V,level+1,arr,x) ) 
						
						!= 
						
						NULL 
					)
				{
					return res;	
				}	
		}
		
		while	(
				( it2 != (arr[level]).end() )	

				&&

				(it2 != it3)

				&&

				( hasheq(it2,it3,n,k,level) == 1 )

			)
		{
				x = xor_hashes(it2,it3,level+1);

				if 	( 
						(res = wagner(n,k,I,V,level+1,arr,x) ) 
						
						!= 
						
						NULL 
					)
				{
					return res;	
				}	

				it2++;
		}

		return NULL;

	}

	else if ( level == k-1 )
	{
	//	cout << "In Level " << level << endl;

//		print_hash(ins->hash);

		it3 = (arr[level]).find(ins->hash);					
		
		map<string,sol*> last;	

		map<string,sol*>::iterator it4;

		last.insert(last.begin(),std::pair<string,sol*>(ins->hash,ins) );

		it4 = last.begin();
			

		if ( 
				( it3 != (arr[level]).end() )

				&&

				( unique_hashes(it3->second,it4->second) == 1 )

		   )


		{
	//		cout << "Test succeeded" << endl;
			
			x = xor_hashes(it3,it4,level+1);

//			printf("it4->second->sol_ref == it3->second->sol_ref:%d\n",it4->second->sol_ref == it3->second->sol_ref);

			if 	( 
					(res = wagner(n,k,I,V,level+1,arr,x) ) 
					
					!= 
					
					NULL 
				)
			{
				return res;	
			}	
				
		}

		else
		{
		//	cout << "Level " << level << " check failed" << endl;

			(arr[level]).insert( arr[level].begin(),std::pair<string,sol*>(ins->hash,ins) );
		}


	}

	else // level == k
	{
//		cout << "Level k" << endl;
		
		return &(ins->sol_ref);
	}	
	
}

int verify	(	
		
		unsigned char I[17],

		unsigned char V[21],

		unsigned n,

		unsigned k,

		vector<string*> * input
	  	)

{
	unsigned char blake_hash[(unsigned long long int)ceil(n/8.0)];
	
	unsigned char output_hash[(unsigned long long int)ceil(n/8.0)];

	memset(output_hash,0x00,(unsigned long long int)ceil(n/8.0)*sizeof(unsigned char));

	string base = char_arr_to_string(I,17) + char_arr_to_string(V,21);

	string hash_input = "";

	map<string,bool> hunt_duplicate;

	unsigned long long int i = 0, j = 0;

	while ( i < (*input).size() )
	{
		hash_input.clear();

		hash_input = base + ( *((*input)[i]) );

		if ( hunt_duplicate.find(hash_input) != hunt_duplicate.end() )
		{
			cout << "Duplicate found" << endl;
			
			return 0;
		}

		else
		{
			hunt_duplicate.insert(hunt_duplicate.begin(), std::pair<string,bool>(hash_input,1) );
		}
	
		crypto_generichash(blake_hash,((unsigned long long int)ceil(n/8.0))*sizeof(unsigned char),&(hash_input.front()),hash_input.size(),NULL,0);
		
		blake_hash[0] = last_l_bits(blake_hash[0],n%8); // ( (0xff) & ( 0xff >> (8-n%8) ) );

		j = 0;

		while ( j < (unsigned long long int)ceil(n/8.0) )
		{
			output_hash[j] ^= blake_hash[j];

			j++;
		}

		i++;
	}	

	i = 0;

	while ( i < (unsigned long long int)ceil(n/8.0) )
	{
		if ( output_hash[i] != 0 )
		{
			return 0;
		}

		i++;
	}

	return 1;

}

#if 0
int verify	(	
		
		unsigned char I[17],

		unsigned char V[21],

		unsigned n,

		unsigned k,

		vector<string*> * input
	  	)

{
	unsigned mlen = (unsigned)ceil( ceil(n/(k+1.0)+1.0) / 8.0 ) + 16 + 20 + 1;

	unsigned rounds = (unsigned)pow(2,k);

	unsigned i = 0, j = crypto_generichash_BYTES_MAX-1, l = 0, r = 0;

	const unsigned solbound = (unsigned)ceil( ceil( n / (k + 1.0) + 1.0 ) / 8.0 );

	unsigned char message[mlen];

	unsigned char hash[65];

	unsigned char out[65];

	memset(hash,0x0,65*sizeof(unsigned char));

	memset(out,0x0,65*sizeof(unsigned char));

	memset(message,0x0,mlen*sizeof(unsigned char));

	if 	( 	mlen

			<

			(unsigned)ceil( ( pow(2,k) * ( n / (k + 1.0) + 1.0 ) ) / 8.0 )

		)

	{
		fprintf(stderr,"Error: Length of solution byte array too short!\n");

		exit(1);
	}

	if	(
			 d > 512 
		)
	{
		fprintf(stderr,"Error: Blake2b hash outputs are meant to be 512 bits long. So setting difficulty d to be greater than that would cause undefined behavior. Exiting\n");

		exit(1);
	}	

		
	while ( r < rounds )
	{
		strncat(message,I,16);

		strncat(message,V,20);

		strncat(message,&x[k],solbound);

		l += solbound;

		crypto_generichash	(	
				
					hash,
				
					crypto_generichash_BYTES_MAX,

					message,

					(size_t)mlen,

					NULL,

					0

					)
		
		;

		while ( i < j )
		{
			out[i] ^= hash[i];

			if 	(
					( r == rounds - 1 )

					&&

					( out[i] != 0 )
			   	)
			{
				return 0;
			}

			out[j] ^= hash[j];

			if 	(

					( r == rounds - 1 )

					&&

					( out[j] != 0 )
			   	)
			{
				return 0;
			}
		
			i++; j--;	
		}

		memset(message,0x0,mlen * sizeof(unsigned char));
		
		r++;
	}

	unsigned dlen = 16 + 20 + (unsigned)pow(2,k) * solbound;

	unsigned char dmessage[dlen];

	strncat(dmessage,I,16);


	strncat(dmessage,V,20);

	r = 0; l = 0;

	while ( r < rounds )
	{
		strncat(dmessage,&x[l],solbound);

		r++;

		l += solbound;

	}

	crypto_generichash	(	
				
				hash,

				crypto_generichash_BYTES_MAX,

				dmessage,

				(size_t)dlen,

				NULL,

				0

				)

	;

	// Now checking if the leading d bits are zero
	
	unsigned d_bytes = d/8;

	unsigned d_i = 0;

	unsigned char verification = 0x00;

	while 	( d_i < d_bytes )
	{
		verification |= hash[d_i];

		if ( verification != 0x00 )
		{
			return 0;
		}
		
		d_i++;
	}	

	// Now the last byte must be checked for remaining amount of zero bits
	
	unsigned xrem = d - ( 8 * d_bytes );

	unsigned x_l = 8 - xrem;

	if	( hash[d_i] >= (unsigned char)pow(2,x_l) )
	{
			return 0;	
	}	

	return 1;

}
#endif

int main(int argc,char* argv[])
{
	if ( sodium_init() < 0 )
	{
		fprintf(stderr,"Error: Failed to initialize libsodium. Exiting\n");

		exit(1);
	}
/*	
	unsigned char k1[16];
	
	unsigned char k2[16];

	unsigned char * test_bloom = 0;

	unsigned long long i = 0;

	randombytes_buf(k1,16*sizeof(unsigned char));
	
	randombytes_buf(k2,16*sizeof(unsigned char));
	
	unsigned long long num_found = 0;	
	
	while ( i < 32000000 )
	{	
		solution(&test_bloom,k1,k2,67,2,&num_found);

		i++;
	}

	printf("Number of collisions found:%u\n",num_found);

	free(test_bloom);

	unsigned char I[17], V[21];

	randombytes_buf(I,17);

	randombytes_buf(V,21);

	unsigned long long int n = strtoull(argv[1],0x00,10), k = strtoull(argv[2],0x00,10);
		
	map<string,sol*> arr[k+1];	
	
	string t = "", h = "";

	sol * x = 0;
		
	unsigned char buf[(unsigned long long int)y];

	memset(buf,0x0,(unsigned long long int)y*sizeof(unsigned char));

	wagner(n,k,I,V,0,t,h,x,buf,arr);

*/

#if 0
	// Testing char_arr_to_string
	
	unsigned char arr[8] = {0x12,0x34,0x56,0x78,0x90,0x12,0x34,0x00};

	string ans = char_arr_to_string(arr,8);

	unsigned long long int i = 0;

	unsigned char x = 0x90;

	while ( i < ans.size() )
	{
		printf("%.2x ",ans[i]);

		i++;
	}

	putchar(0x0a);

	i = 0;

	while ( i < ans.size() )
	{
		printf("first_l_bits: %.2x\n",first_l_bits(ans[i],4));

		printf("last_l_bits: %.2x\n",last_l_bits(ans[i],4));

		i++;
	}

#endif

#if 0

	// Testing num_to_string() and which_endian() functions
	
	unsigned long long int n = strtoull(argv[1],0x00,10), k = strtoull(argv[2],0x00,10);
	
	string test = "";

	unsigned long long int i = 0, j = 0;

	while ( j < (unsigned long long int)ceil(pow(2,n/(k+1)+1)) )
	{
		test = num_to_string(j,n,k);

		while ( i < test.size() )
		{
			printf("%x ",test[i]);

			i++;
		}

		putchar(0x0a);

		i = 0;

		j++;
	}
#endif

#if 0
	// Testing num_to_hash function
	unsigned long long int i = 0;
	
	unsigned long long int n = strtoull(argv[1],0x00,10), k = strtoull(argv[2],0x00,10);
	
	unsigned long long int N = pow(2,ceil(n/(k+1))+1);

	string stringnum = "", hashstring = "";

	while ( i <  N )
	{
		stringnum = num_to_string(i,n,k);

		hashstring = num_to_hash(stringnum,n);

		print_hash(hashstring);

		i++;
	}
#endif
	// Testing wagner function
	
	unsigned char I[17], V[21];

	randombytes_buf(I,17);

	randombytes_buf(V,21);
	
	unsigned long long int n = strtoull(argv[1],0x00,10), k = strtoull(argv[2],0x00,10);
	
	map<string,sol*> arr[k+1];	

	map<string,sol*>::iterator it1, it2;

	vector<string*> * ans = wagner(n,k,I,V,0,arr,0x00); // Test for level 0 successful

	printf("%p\n",ans);

	unsigned long long int i = 0, j = 0;

	cout << "Solution strings in hexadecimal form" << endl;

	if ( ans == NULL )
	{
		return 0;
	}

	while (
		        ( ans != 0x00 )

			&&

			( i < (*ans).size() )
	      )
	{
		j = 0;

		while ( j < (*((*ans)[i])).size() )
		{
			printf("%.2x",(*((*ans)[i]))[j]);
			
			j++;
		}

		putchar(0x0a);

		i++;
	}

	printf("verify boolean == %d\n",verify(I,V,n,k,ans));
	
	return 0;
}
