import nacl.hash
import nacl.encoding
import os
import time

# https://snov.io/email-verifier

def gen_mac():

    secret_key = os.urandom(16)

    data = bytes('Chocolate','utf8')

    salt = os.urandom(16)

    hmac = nacl.hash.siphashx24(data,key=secret_key,encoder=nacl.encoding.URLSafeBase64Encoder)

    print(hmac)

i = 0

t0 = time.time()

while i < 30:
    
    gen_mac()

#    time.sleep(0.25)

    i += 1

t1 = time.time()

print((t1-t0))
