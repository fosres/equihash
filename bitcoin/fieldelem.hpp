#ifndef __CSTDIO__
#define __CSTDIO__ 1
#include <cstdio>
#endif
#ifndef __CSTDBOOL__
#define __CSTDBOOL__ 1
#include <cstdbool>
#endif
#ifndef __IOSTREAM__
#define __IOSTREAM__ 1
#include <iostream>
#endif
#ifndef __STRING__
#define __STRING__ 1
#include <string>
#endif
#ifndef __STDEXCEPT__
#define __STDEXCEPT__ 1
#include <stdexcept>
#endif
#ifndef __GMP__
#define __GMP__ 1
#include <gmp.h>
#endif
#ifndef __MATH__
#define __MATH__ 1
#include <cmath>
#endif

#ifndef __NAMESPACE_STD__
#define __NAMESPACE_STD__ 1
using namespace std;
#endif 

class fieldelem
{
	public:
		mpz_t num;

		mpz_t prime;

		unsigned char bnum = 0;

		unsigned char bprime = 0;

		fieldelem(void)
		{
			mpz_init(num);

			mpz_init(prime);
		}

		fieldelem(unsigned long int n,unsigned long int p)
		{
			mpz_init(num);

			mpz_init(prime);

			mpz_set_ui(num,n);

			mpz_set_ui(prime,p);

			bnum = 10;

			bprime = 10;
		}

		fieldelem(const char * n, unsigned char bn, const char * p, unsigned char bp)
		{
			mpz_init(num);

			mpz_init(prime);

			bnum = bn;

			bp = bprime;

			if ( mpz_set_str(num,n,bnum) == -1 )
			{
				throw std::runtime_error("number string n is not a valid number in base bnum\n");	
			}

			if ( mpz_set_str(prime,p,bprime) == -1 )
			{
				throw std::runtime_error("number string p is not a valid number in base bprime\n");	
			}

			if ( 
				( mpz_cmp(num,prime) == 1)
				
				||
				
				( mpz_cmp(num,prime) == 0)

				||

				( mpz_cmp_ui(num,0) == -1)

				||
				
				( mpz_cmp_ui(prime,0) == 0)

			   )
			{
				throw std::runtime_error("Error: n >= p or n < 0 or p == 0\n");	
			}

		}
		
		fieldelem(mpz_t n,mpz_t p)
		{
			mpz_init(num);

			mpz_init(prime);

			bnum = 10;

			bprime = 10;

			if ( 
				( mpz_cmp(n,p) == 1 )

				||

				( mpz_cmp(n,p) == 0 )
			
				||

				( mpz_cmp_ui(n,0) == -1)

				||
				
				( mpz_cmp_ui(p,0) == -1)
				
				||
				
				( mpz_cmp_ui(p,0) == 0)

			   )
			{
				throw std::runtime_error("Error: n >= p or n < 0 or p == 0\n");	
			}
			
			mpz_set(num,n);

			mpz_set(prime,p);
		}
		
		~fieldelem()
		{
			mpz_clear(num);

			mpz_clear(prime);
		}
		
		void print(void)
		{
			gmp_printf("FieldElement_%Zd(%Zd)\n",prime,num);
		}

		
		fieldelem operator+(fieldelem r2)
		{
			if ( mpz_cmp(prime,r2.prime) != 0 )
			{
				gmp_printf("prime: %Zd\n",prime);

				gmp_printf("r2.prime: %Zd\n",r2.prime);

				throw std::runtime_error("Error: Cannot add two numbers that are not part of the same field. Their primes must equal.\n");

			}

			fieldelem x = fieldelem();

			mpz_add(x.num,num,r2.num);

			mpz_mod(x.num,x.num,prime);

			mpz_set(x.prime,prime);

			return x;
			
		}

		fieldelem operator-(fieldelem r2)
		{
			if ( mpz_cmp(prime,r2.prime) != 0 )
			{
				throw std::runtime_error("Error: Cannot subtract two numbers that are not part of the same field. Their primes must equal.\n");

			}
			
			fieldelem x = fieldelem();

			mpz_set(x.prime,prime);

			mpz_sub(x.num,num,r2.num);

			mpz_mod(x.num,x.num,prime);
			
			if ( mpz_cmp_ui(x.num,0) == -1 )
			{
				mpz_add(x.num,x.num,prime);
			}

			return x;
			
		}

		fieldelem operator*(fieldelem r2)
		{
			if ( mpz_cmp(prime,r2.prime) != 0 )
			{
				gmp_printf("prime: %Zd\n",prime);

				gmp_printf("r2.prime: %Zd\n",r2.prime);

				throw std::runtime_error("Error: Cannot multiply two numbers that are not part of the same field. Their primes must equal.\n");

			}
			
			fieldelem product = fieldelem();

			mpz_mul(product.num,num,r2.num);

			mpz_mod(product.num,product.num,prime);

			mpz_set(product.prime,prime);

			return product;
		
		}
		
		fieldelem operator*(signed long int r2)
		{
			fieldelem product = fieldelem();

			mpz_mul_si(product.num,num,r2);

			mpz_mod(product.num,product.num,prime);

			mpz_set(product.prime,prime);

			return product;
		
		}

		fieldelem operator/(fieldelem r2)
		{
			if ( mpz_cmp(prime,r2.prime) != 0 )
			{
				throw std::runtime_error("Error: Cannot divide two numbers that are not part of the same field. Their primes must equal.\n");

			}

			mpz_t x, y, z;

			mpz_init(x);

			mpz_init(y);

			mpz_init(z);

			mpz_set_ui(y,0);

			mpz_set(z,r2.prime);

			mpz_sub_ui(z,z,2);

			mpz_set_ui(x,1);

			while ( mpz_cmp(y,z) != 0 )
			{
				mpz_mul(x,x,r2.num);

				mpz_add_ui(y,y,1);
			}				

			mpz_mul(x,x,num);

			mpz_mod(x,x,prime);

			fieldelem res = fieldelem(x,prime);

			mpz_clear(x);

			mpz_clear(y);

			mpz_clear(z);
			
			return res;
		
		}

		fieldelem operator^(signed long int e)
		{
			mpz_t n, p_1;

			mpz_init(n);

			mpz_init(p_1);

			if ( e >= 0 )
			{
				mpz_set(n,num);

				mpz_powm_ui(n,n,e,prime);
			}

			else
			{

				mpz_set_si(n,e);	

				mpz_set(p_1,prime);

				mpz_sub_ui(p_1,p_1,1);

				gmp_printf("p_1: %Zd\n",p_1);

				mpz_mod(n,n,p_1);

				if ( mpz_cmp_ui(n,0) == -1 )
				{
					mpz_add(n,n,p_1);	

					gmp_printf("mpz_add(n,n,p_1) == %Zd\n",n);
				}

				mpz_powm(n,num,n,prime);

			}
			
			fieldelem res = fieldelem(n,prime);
			
			mpz_clear(n);

			mpz_clear(p_1);

			return res;
			
		}
		
		bool operator==(fieldelem r2)
		{
			if ( mpz_cmp(prime,r2.prime) != 0 )
			{
				return 0;
			}

			return ( mpz_cmp(num,r2.num) == 0 ) && ( mpz_cmp(prime,r2.prime) == 0 );
		}

		bool operator!=(fieldelem r2)
		{
			if ( mpz_cmp(prime,r2.prime) != 0 )
			{
				return 0;
			}

			return ( mpz_cmp(num,r2.num) != 0 ) || ( mpz_cmp(prime,r2.prime) != 0 );

		}

};
