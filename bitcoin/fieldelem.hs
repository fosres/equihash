module Fieldelem where
import Data.Bits

type Field = (Integer,Integer) 

field_init :: Integer -> Integer -> Field

field_init num prime =
 if ( num >= prime || num < 0 || prime < 0 )
  then error "num is not in field range"
 else (num,prime)

s256field_init :: Integer -> Field

s256field_init n =
 field_init n (2^256-2^32-977) 
 


fieldadd :: Field -> Field -> Field

fieldadd a b = 
 if ( (snd a) /= (snd b) )
  then
   error "Cannot add two Fields with different primes"
 else if ( ( ( (fst a) >= (snd a) ) || ( (fst a) < 0 ) )  || ( ( fst b >= (snd b) ) || ( (fst b) < 0 ) ) || ( snd a /= snd b ) )
  then error "Error: num >= prime or num < 0"
 else
  ((mod) (fst a + fst b) (snd a),snd a)

fieldsub :: Field -> Field -> Field

fieldsub a b = 
 if ( ( ( (fst a) >= (snd a) ) || ( (fst a) < 0 ) )  || ( ( fst b >= (snd b) ) || ( (fst b) < 0 ) ) || ( snd a /= snd b ) )
  then error "Error: num >= prime or num < 0"
 else
  ((mod) (fst a - fst b) (snd a),snd a)

fieldmult :: Field -> Field -> Field 

fieldmult a b =
 if ( ( ( (fst a) >= (snd a) ) || ( (fst a) < 0 ) )  || ( ( fst b >= (snd b) ) || ( (fst b) < 0 ) ) || ( snd a /= snd b ) )
  then error "Error: num >= prime or num < 0"
 else
  ( (mod) ( (fst a) * (fst b) ) (snd a) , snd a )

fieldmultint :: Field -> Integer -> Field

fieldmultint a b =
 if ( ( ( (fst a) >= (snd a) ) || ( (fst a) < 0 ) ) )
  then 
   error "Error: num >= prime or num < 0"
 else
  ( (mod) ( (fst a) * (b) ) (snd a) , snd a )
  
  

fieldeq :: Field -> Field -> Bool

fieldeq a b =
 if ( not ( snd a == snd b ) )
  then error "Error: Primes do not match!\n"
 else if ( (fst a /= fst b) )
  then False
 else
  True  

fieldneq :: Field -> Field -> Bool

fieldneq a b =
 if ( not ( snd a == snd b ) )
  then error "Error: Primes do not match!\n"
 else if ( (fst a == fst b) )
  then False 
 else
  True 

field_mod_exp :: Field -> Integer -> Integer -> Integer -> Field

--b == base mod modulus (aka prime)
--e == (mod) (e) ( (snd a) - 1) :: Integer
--Passes the argument as the above
--Pass as field_mod_exp a 1 (e) 1
--num == b
--Fast modular exponentiation is taken from:
--https://en.wikipedia.org/wiki/Modular_exponentiation#Right-to-left_binary_method

field_mod_exp a b e result =
 if ( ( ( (fst a) >= (snd a) ) || ( (fst a) < 0 ) ) )
  then error "Error: b of base >= prime or b < 0"
 else if e == 0
  then field_init (result) (snd a)
 else if ( ( (.&.) e 1 ) == 1)
  then field_mod_exp a ( mod (b*b) (snd a) ) (shiftR e 1) ( mod (result * b) (snd a) )
 else if ( ( (.&.) e 1 ) == 0)
  then field_mod_exp a ( mod (b*b) (snd a) ) (shiftR e 1) (result)
 else
  error "All cases for field_mod_exp failed!\n"

fieldpow :: Field -> Integer -> Field

fieldpow a e =
 if ( ( ( (fst a) >= (snd a) ) || ( (fst a) < 0 ) ) )
  then error "Error: num of base >= prime or num < 0"
 else
  field_mod_exp a ( mod (fst a) (snd a) ) n 1 where n = (mod) (e) ( (snd a) - 1) :: Integer
  --( (mod) ( (fst a)^(n) ) (snd a) , snd a) where n = (mod) (e) ( (snd a) - 1) :: Integer

fieldiv :: Field -> Field -> Field

fieldiv a b =
 if ( ( ( (fst a) >= (snd a) ) || ( (fst a) < 0 ) )  || ( ( fst b >= (snd b) ) || ( (fst b) < 0 ) ) || ( snd a /= snd b ) )
  then error "Error: num >= prime or num < 0"
 else
  fieldmultint (fieldpow b ( (snd b) - 2 ) ) (fst a)

{-
fieldiv a b =
 if ( ( ( (fst a) >= (snd a) ) || ( (fst a) < 0 ) )  || ( ( fst b >= (snd b) ) || ( (fst b) < 0 ) ) || ( snd a /= snd b ) )
  then error "Error: num >= prime or num < 0"
 else
  ( (mod) ( (*) (fst a) ( (fst b)^( (snd b) - 2) ) ) (snd b) , snd b)
-} 

{-
point_init :: Field -> Field -> Field -> Field -> Point

point_init x y a b =
 if ( fieldneq (fieldpow y 2) ( fieldadd (fieldpow x 3) (  
-}
