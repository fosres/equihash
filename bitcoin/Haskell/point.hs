module Point where
import Data.Bits
import Fieldelem
import Signature

type Point = [Field]

point_init :: Field -> Field -> Field -> Field -> Point

point_init x y a b =
 if ( fieldeq x y ) && ( (fst x) == 0 ) && ( (fst y) == 0 )
  then
   [x,y,a,b]
 else if ( fieldneq (fieldpow y 2) ( fieldadd ( fieldadd (fieldpow x 3) ( fieldmult a x ) ) (b) ) )
  then
   error "point_init: Point is not on curve"
 else
  [x,y,a,b]

s256point_init :: Field -> Field -> Point

s256point_init x y =
 point_init x y (s256field_init 0) (s256field_init 7)

point_init_int :: Integer -> Integer -> Integer -> Integer -> Integer -> Point

point_init_int x y a b p =
  let px = field_init x p
      py = field_init y p
      pa = field_init a p
      pb = field_init b p
      in point_init px py pa pb

s256point_init_int :: Integer -> Integer -> Point

s256point_init_int x y =
 s256point_init (s256field_init x) (s256field_init y)

pointeq :: Point -> Point -> Bool 

pointeq a b =
    (  a !! 0 ) == ( b !! 0 )
    
    &&
 
    ( a !! 1 ) == ( b !! 1 )

    &&

    ( a !! 2 ) == ( b !! 2 )

    &&

    ( a !! 3 ) == ( b !! 3  )
       
pointneq :: Point -> Point -> Bool 

pointneq a b =
 not (pointeq a b)

pointadd :: Point -> Point -> Point

pointadd op1 op2 =
 let op1x = op1 !! 0
     op1y = op1 !! 1
     op1a = op1 !! 2
     op1b = op1 !! 3
     op2x = op2 !! 0
     op2y = op2 !! 1
     op2a = op2 !! 2
     op2b = op2 !! 3
     in
      if (fieldneq op1a op2a) || (fieldneq op1b op2b)
       then
        error "Points are not on the same curve"
      else if (pointeq op1 op2) && (fieldeq op1y (fieldmultint op1x 0) )
       then point_init_int 0 0 (fst op1a) (fst op1b) (snd op1x)
      else if (fieldeq op1x op2x) && (fieldneq op1y op2y)
       then point_init_int 0 0 (fst op1a) (fst op1b) (snd op1x)  
      else if (fst op1x) == 0 && (fst op1y) == 0 --op1 is at Infinity
       then
        op2
      else if (fst op2x) == 0 && (fst op2y) == 0 --op2 is at Infinity
       then
        op1
      else if (pointneq op1 op2) -- P1 /= P2 
       then
        let s = ( fieldiv (fieldsub op2y op1y) (fieldsub op2x op1x) )
            x3 = (fieldsub (fieldsub (fieldpow s 2) (op1x) ) op2x)
            y3 = (fieldsub (fieldmult (s) (fieldsub op1x x3) ) (op1y) )
            in [x3,y3,op1a,op1b]
      else if (pointeq op1 op2) -- P1 == P2
       then
        let s = ( fieldiv (fieldadd (fieldmultint (fieldpow op1x 2) (3) ) (op1a) ) (fieldmultint op1y 2) )
            x3 = (fieldsub (fieldpow s 2) (fieldmultint op1x 2) )
            y3 = (fieldsub (fieldmult (s) (fieldsub op1x x3) ) (op1y) )
            in [x3,y3,op1a,op1b]
      else error "All cases for point addition failed!"

{-
pointmul :: Point -> Point -> Integer -> Point

pointmul x a b =
 if b == 1
  then
   x
 else
  (pointmul (pointadd x a) (a) (b-1))

pointmult :: Point -> Integer -> Point

pointmult x a =
 pointmul x x a
-}

rmul :: Integer -> Point -> Point -> Point

--Try this binary exponentiation algorithm: https://cp-algorithms.com/algebra/binary-exp.html

rmul coef current result =
  if coef == 0
   then
    result
  else if ( ( (.&.) coef 1 ) == 1 )
   then
    rmul (shiftR coef 1) (pointadd current current) (pointadd result current)
  else
   rmul (shiftR coef 1) (pointadd current current) (result)

pointmult :: Point -> Integer -> Point

pointmult current coef =
 let res = point_init_int 0 0 ( fst (current !! 2) ) ( fst (current !! 3) ) (snd (current !! 0) )
  in rmul coef current res 

s256pointmult :: Point -> Integer -> Point

s256pointmult current coefficient =
 let n = 0xfffffffffffffffffffffffffffffffebaaedce6af48a03bbfd25e8cd0364141 :: Integer
     coef = mod coefficient n :: Integer
     in pointmult current coef

gx = 0x79be667ef9dcbbac55a06295ce870b07029bfcdb2dce28d959f2815b16f81798 :: Integer

gy = 0x483ada7726a3c4655da4fbfc0e1108a8fd17b448a68554199c47d08ffb10d4b8 :: Integer

g = s256point_init_int gx gy

n = 0xfffffffffffffffffffffffffffffffebaaedce6af48a03bbfd25e8cd0364141 :: Integer


verify :: Point -> Integer -> Signature -> Bool

verify a z sig =
 let sinv = ( fst (fieldpow (field_init (snd sig) (n) ) (n-2) ) )
     --u = (mod ( (sinv) * z) (n) )     
     u = (mod ( (mod sinv n) * (mod z n) ) (n) )     
     --v = (mod ( (fst sig) * sinv ) (n) )
     v = (mod ( (mod (fst sig) n) * (mod sinv n) ) (n) )
     total = (pointadd (s256pointmult (g) (u)) (s256pointmult (a) (v) ) )
     in ( (fst (total !! 0) ) == (fst sig) )


--gen256 1 0
gen256 :: Integer -> Integer ->  Integer

gen256 i x =
 if (i+1) == 256
  then (.|.) (x) (1)
 else gen256 (i+1) ( shiftL ( (.|.) (x) (1) ) (1) )  

mod256max :: Integer

mod256max = gen256 1 0
 
